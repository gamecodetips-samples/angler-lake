﻿using UnityEngine;

namespace Assets.Scripts.Fishing
{
    public struct FishStats
    {
        public string Id { get; private set; }
        public string Name { get; private set; }
        public Sprite Photo { get; private set; }
        public float Weight { get; private set; }
        public float FishZone { get; private set; }
        public float EscapeRate { get; private set; }
        public float CatchRate { get; private set; }
        public float MaxMoveSpeed { get; private set; }
        public float LineDegradation { get; private set; }
        public float ActivityFactor { get; private set; }

        public static FishStats Create(string id, string name, Sprite photo, float weight, float fishZone, float escapeRate, float catchRate,
            float maxMoveSpeed, float lineDegradation, float activityFactor)
        {
            return new FishStats
            {
                Id = id,
                Name = name,
                Photo = photo,
                Weight = weight,
                FishZone = fishZone,
                EscapeRate = escapeRate,
                CatchRate = catchRate,
                MaxMoveSpeed = maxMoveSpeed,
                LineDegradation = lineDegradation,
                ActivityFactor = activityFactor
            };
        }
    }
}
