﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Common;

namespace Assets.Scripts.Minigame
{
    [Serializable]
    public class MinigameSfx
    {
        public VariableSfx ReelInSfx;
        public VariableSfx ReelIdleSfx;
        public VariableSfx LineTensionSfx;
        public VariableSfx FishChangeSfx;
    }
}
